CREATE SCHEMA `meep` DEFAULT CHARACTER SET utf8 ;

USE `meep`;

CREATE TABLE IF NOT EXISTS `rental_vehicle` (
  `id` VARCHAR(255) NOT NULL,
  `deleted` TINYINT(1) NULL,
  `sync_instance_id` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));
package com.meep.pollingapp.core.boundary.service;

import com.meep.pollingapp.core.domain.RentalVehicle;
import com.meep.pollingapp.core.domain.geo.DelimitedArea;
import java.util.List;

public interface IExternalRentalItemService {

    List<RentalVehicle> getRentalVehicles(DelimitedArea delimitedArea, List<String> companyIds);
}

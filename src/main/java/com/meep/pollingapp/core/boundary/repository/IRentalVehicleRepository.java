package com.meep.pollingapp.core.boundary.repository;

import com.meep.pollingapp.core.domain.RentalVehicle;
import java.util.List;
import java.util.Optional;

public interface IRentalVehicleRepository {

    Optional<RentalVehicle> save(RentalVehicle rentalVehicle);

    Optional<RentalVehicle> findById(String rentalVehicleId);

    Optional<RentalVehicle> findByIdAndActive(String rentalVehicleId);

    List<RentalVehicle> saveAll(List<RentalVehicle> rentalVehicles);

    List<RentalVehicle> findNotUpdatedInCurrentSync(String syncInstanceId);
}

package com.meep.pollingapp.core.domain.geo;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Builder
@Value
public class DelimitedArea {

        @NonNull
        CartesianPoint lowerLeftLatLon;
        @NonNull
        CartesianPoint upperRightLatLon;
}

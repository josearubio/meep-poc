package com.meep.pollingapp.core.domain.geo;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Builder
@Value
public class CartesianPoint {
    @NonNull
    double x;
    @NonNull
    double y;

    public String toString(){
        return x + "," + y;
    }
}

package com.meep.pollingapp.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RentalVehicle {

        String id;
        String name;
        Float x;
        Float y;
        String licencePlate;
        Integer range;
        Integer batteryLevel;
        Integer seats;
        String model;
        String resourceImageId;
        Boolean realTimeData;
        String resourceType;
        String companyZoneId;
        String syncInstanceId;
        Boolean deleted;
}

package com.meep.pollingapp.application.configuration;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

@Configuration
public class SyncRentalVehiclesJobConfig {

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Bean("asyncTaskExecutor")
    public JobLauncher asyncJobLauncher() {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(jobRepository);
        jobLauncher.setTaskExecutor(new SimpleAsyncTaskExecutor());
        return jobLauncher;
    }

    @Bean("syncRentalVehiclesJob")
    public Job syncRentalVehiclesJob(
            @Qualifier("syncRentalVehiclesStep") Step syncRentalVehiclesStep,
            @Qualifier("deleteRentalVehiclesStep") Step deleteRentalVehiclesStep
        ) {

        return this.jobBuilderFactory.get("syncRentalVehiclesJob")
            .start(syncRentalVehiclesStep)
            .next(deleteRentalVehiclesStep)
            .build();
    }
}

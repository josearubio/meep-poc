package com.meep.pollingapp.application.configuration;

import com.meep.pollingapp.core.domain.RentalVehicle;
import com.meep.pollingapp.infrastructure.batch.DeleteRentalVehiclesTaskLet;
import com.meep.pollingapp.infrastructure.batch.RentalVehicleProcessor;
import com.meep.pollingapp.infrastructure.batch.RentalVehicleWriter;
import com.meep.pollingapp.infrastructure.batch.RestRentalVehicleReader;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SyncRentalVehiclesStepConfig {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public ItemReader<RentalVehicle> getRentalVehicleReader() {
        return new RestRentalVehicleReader();
    }

    @Bean
    public ItemProcessor<RentalVehicle, RentalVehicle> getRentalVehicleProcessor() {
        return new RentalVehicleProcessor();
    }

    @Bean
    public ItemWriter<RentalVehicle> getRentalVehicleWriter() {
        return new RentalVehicleWriter();
    }

    @Bean
    public DeleteRentalVehiclesTaskLet deleteRentalVehiclesTaskLet() {
        return new DeleteRentalVehiclesTaskLet();
    }

    @Bean
    public Step syncRentalVehiclesStep() {

        return stepBuilderFactory.get("syncRentalVehiclesStep").<RentalVehicle, RentalVehicle> chunk(100)
            .reader(getRentalVehicleReader())
            .processor(getRentalVehicleProcessor())
            .writer(getRentalVehicleWriter())
            .build();
    }

    @Bean
    public Step deleteRentalVehiclesStep() {

        return stepBuilderFactory.get("deleteRentalVehiclesStep")
            .tasklet(deleteRentalVehiclesTaskLet())
            .build();
    }
}

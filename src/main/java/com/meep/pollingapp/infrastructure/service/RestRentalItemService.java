package com.meep.pollingapp.infrastructure.service;

import com.meep.pollingapp.core.boundary.service.IExternalRentalItemService;
import com.meep.pollingapp.core.domain.RentalVehicle;
import com.meep.pollingapp.core.domain.geo.DelimitedArea;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class RestRentalItemService implements IExternalRentalItemService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${polling.url}")
    private String apiUrl;

    @Override
    public List<RentalVehicle> getRentalVehicles(DelimitedArea delimitedArea, List<String> companyIds) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(apiUrl)
            .queryParam("lowerLeftLatLon",delimitedArea.getLowerLeftLatLon().toString())
            .queryParam("upperRightLatLon", delimitedArea.getUpperRightLatLon().toString())
            .queryParam("companyZoneIds", String.join(",", companyIds));

        HttpEntity<?> entity = new HttpEntity<>(headers);

        ResponseEntity<List<RentalVehicle>> response = restTemplate.exchange(
            builder.toUriString(),
            HttpMethod.GET,
            entity,
            new ParameterizedTypeReference<List<RentalVehicle>>() {});

        return response.getBody();
    }
}

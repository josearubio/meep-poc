package com.meep.pollingapp.infrastructure.adapter;

import com.meep.pollingapp.core.boundary.repository.IRentalVehicleRepository;
import com.meep.pollingapp.core.domain.RentalVehicle;
import com.meep.pollingapp.infrastructure.persistence.entity.RentalVehicleEntity;
import com.meep.pollingapp.infrastructure.persistence.repository.RentalVehicleJPARepository;
import java.util.List;
import java.util.Optional;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RentalVehicleRepositoryAdapter implements IRentalVehicleRepository {

    @Autowired
    private RentalVehicleJPARepository rentalVehicleRepository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public Optional<RentalVehicle> save(RentalVehicle rentalVehicle) {
        RentalVehicleEntity rentalVehicleEntity = rentalVehicleRepository.save(mapper.map(rentalVehicle, RentalVehicleEntity.class));

        return Optional.ofNullable(mapper.map(rentalVehicleEntity, RentalVehicle.class));
    }

    @Override
    public Optional<RentalVehicle> findById(String rentalVehicleId) {
        Optional<RentalVehicleEntity> rentalVehicleEntity = rentalVehicleRepository.findById(rentalVehicleId);

        RentalVehicle rentalVehicle = mapFromOptional(rentalVehicleEntity);

        return Optional.ofNullable(rentalVehicle);
    }

    @Override
    public Optional<RentalVehicle> findByIdAndActive(String rentalVehicleId) {
        Optional<RentalVehicleEntity> rentalVehicleEntity = rentalVehicleRepository.findByIdAndDeletedIsFalse(rentalVehicleId);

        RentalVehicle rentalVehicle = mapFromOptional(rentalVehicleEntity);

        return Optional.ofNullable(rentalVehicle);
    }

    @Override
    public List<RentalVehicle> saveAll(List<RentalVehicle> rentalVehicles) {
        List<RentalVehicleEntity> rentalVehicleEntitiesSaved = rentalVehicleRepository
            .saveAll(mapper.map(rentalVehicles, new TypeToken<List<RentalVehicleEntity>>(){}.getType()));

        return mapper.map(rentalVehicleEntitiesSaved, new TypeToken<List<RentalVehicle>>(){}.getType());
    }

    @Override
    public List<RentalVehicle> findNotUpdatedInCurrentSync(String syncInstanceId) {
        List<RentalVehicleEntity> rentalVehiclesEntities = rentalVehicleRepository.findBySyncInstanceIdIsNotAndDeletedIsFalse(syncInstanceId);

        return mapper.map(rentalVehiclesEntities, new TypeToken<List<RentalVehicle>>(){}.getType());
    }

    private RentalVehicle mapFromOptional(Optional<RentalVehicleEntity> rentalVehicleEntity){
        return rentalVehicleEntity.map(entity -> mapper.map(rentalVehicleEntity.get(), RentalVehicle.class)).orElse(null);
    }

}

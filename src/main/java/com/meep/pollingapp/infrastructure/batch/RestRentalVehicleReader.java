package com.meep.pollingapp.infrastructure.batch;

import com.meep.pollingapp.core.boundary.service.IExternalRentalItemService;
import com.meep.pollingapp.core.domain.RentalVehicle;
import com.meep.pollingapp.core.domain.geo.CartesianPoint;
import com.meep.pollingapp.core.domain.geo.DelimitedArea;
import java.util.Arrays;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RestRentalVehicleReader implements ItemReader<RentalVehicle> {

    @Autowired
    private IExternalRentalItemService rentalItemService;

    private int nextIndex = 0;
    private List<RentalVehicle> data = null;

    @Override
    public RentalVehicle read() {
        if (studentDataIsNotInitialized()) {
            data = fetchData();
        }

        RentalVehicle nextStudent = null;

        if (nextIndex < data.size()) {
            nextStudent = data.get(nextIndex);
            nextIndex++;
        }
        else {
            resetData();
        }

        return nextStudent;
    }

    private void resetData(){
        nextIndex = 0;
        data = null;
    }

    private boolean studentDataIsNotInitialized() {
        return this.data == null;
    }

    private List<RentalVehicle> fetchData() {

        DelimitedArea delimitedArea = DelimitedArea.builder()
            .lowerLeftLatLon(
                CartesianPoint.builder()
                    .x(38.711046)
                    .y(-9.160096)
                    .build()
            )
            .upperRightLatLon(
                CartesianPoint.builder()
                    .x(38.739429)
                    .y(-9.137115)
                    .build()
            )
            .build();

        return rentalItemService.getRentalVehicles(delimitedArea, Arrays.asList("545","467","473"));
    }
}

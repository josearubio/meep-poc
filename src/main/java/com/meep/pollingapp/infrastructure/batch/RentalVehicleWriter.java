package com.meep.pollingapp.infrastructure.batch;

import com.meep.pollingapp.core.boundary.repository.IRentalVehicleRepository;
import com.meep.pollingapp.core.domain.RentalVehicle;
import java.util.List;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

public class RentalVehicleWriter implements ItemWriter<RentalVehicle> {

    @Autowired
    private IRentalVehicleRepository rentalVehicleRepository;

    @Override
    public void write(List<? extends RentalVehicle> list) {
        List<RentalVehicle> rentalVehicles = (List<RentalVehicle>)list;

        rentalVehicleRepository.saveAll(rentalVehicles);
    }
}

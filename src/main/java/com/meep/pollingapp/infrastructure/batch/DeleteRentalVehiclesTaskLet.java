package com.meep.pollingapp.infrastructure.batch;

import com.meep.pollingapp.core.boundary.repository.IRentalVehicleRepository;
import com.meep.pollingapp.core.domain.RentalVehicle;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class DeleteRentalVehiclesTaskLet implements Tasklet {

    @Autowired
    private IRentalVehicleRepository rentalVehicleRepository;

    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) {

        JobExecution jobExecution = chunkContext.getStepContext().getStepExecution().getJobExecution();
        List<RentalVehicle> deletedEntities = rentalVehicleRepository.findNotUpdatedInCurrentSync(jobExecution.getJobParameters().getString("instance_id"));

        deletedEntities.forEach(entity -> {
            log.info(String.format("Rental vehicle with id %s is not more available", entity.getId()));
            entity.setDeleted(Boolean.TRUE);
        });

        saveAll(deletedEntities);

        return RepeatStatus.FINISHED;
    }

    protected void saveAll(List<RentalVehicle> entities) {
        rentalVehicleRepository.saveAll(entities);
    }
}

package com.meep.pollingapp.infrastructure.batch;

import com.meep.pollingapp.core.boundary.repository.IRentalVehicleRepository;
import com.meep.pollingapp.core.domain.RentalVehicle;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
public class RentalVehicleProcessor implements ItemProcessor<RentalVehicle, RentalVehicle> {

    @Autowired
    private IRentalVehicleRepository rentalVehicleRepository;

    private JobExecution jobExecution;

    @BeforeStep
    public void beforeStep(StepExecution stepExecution) {
        jobExecution = stepExecution.getJobExecution();
    }

    @Transactional
    public RentalVehicle process(RentalVehicle rentalVehicleInfo) {

        RentalVehicle rentalVehicle = rentalVehicleRepository.findById(rentalVehicleInfo.getId()).orElse(null);

        if(isNewAvailableVehicle(rentalVehicle)){
            logNewRentalVehicle(rentalVehicleInfo);
        }

        if(Objects.isNull(rentalVehicle)){
            rentalVehicle = new RentalVehicle();
            rentalVehicle.setId(rentalVehicleInfo.getId());
        }

        rentalVehicle.setSyncInstanceId(jobExecution.getJobParameters().getString("instance_id"));
        rentalVehicle.setDeleted(Boolean.FALSE);

        return rentalVehicle;
    }

    private void logNewRentalVehicle(RentalVehicle rentalVehicleInfo){
        log.info(String.format("Rental vehicle with id %s is now available", rentalVehicleInfo.getId()));
    }

    private boolean isNewAvailableVehicle(RentalVehicle rentalVehicle){
        return Objects.isNull(rentalVehicle) || Boolean.TRUE.equals(rentalVehicle.getDeleted());
    }
}

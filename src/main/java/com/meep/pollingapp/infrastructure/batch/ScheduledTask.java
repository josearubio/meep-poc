package com.meep.pollingapp.infrastructure.batch;

import java.util.UUID;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class ScheduledTask {

    @Autowired
    @Qualifier("asyncTaskExecutor")
    private JobLauncher jobLauncher;

    @Autowired
    @Qualifier("syncRentalVehiclesJob")
    private Job syncRentalVehiclesJob;

    @Scheduled(cron = "${task.scheduled.cron}")
    public void executeSynchronization()  throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        JobParametersBuilder parametersBuilder = new JobParametersBuilder();
        parametersBuilder.addString("instance_id", UUID.randomUUID().toString(),true);
        jobLauncher.run(syncRentalVehiclesJob, parametersBuilder.toJobParameters());
    }
}

package com.meep.pollingapp.infrastructure.persistence.repository;

import com.meep.pollingapp.infrastructure.persistence.entity.RentalVehicleEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RentalVehicleJPARepository extends JpaRepository<RentalVehicleEntity, String>{
    Optional<RentalVehicleEntity> findByIdAndDeletedIsFalse(String rentalVehicleId);
    List<RentalVehicleEntity> findBySyncInstanceIdIsNotAndDeletedIsFalse(String syncInstanceId);
}

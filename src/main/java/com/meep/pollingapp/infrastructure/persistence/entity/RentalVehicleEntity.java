package com.meep.pollingapp.infrastructure.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "rental_vehicle")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RentalVehicleEntity {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "sync_instance_id")
    private String syncInstanceId;

    @Column(name = "deleted")
    private Boolean deleted;
}

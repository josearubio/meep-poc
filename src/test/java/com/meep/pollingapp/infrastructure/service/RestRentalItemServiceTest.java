package com.meep.pollingapp.infrastructure.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import com.meep.pollingapp.core.domain.RentalVehicle;
import com.meep.pollingapp.core.domain.geo.CartesianPoint;
import com.meep.pollingapp.core.domain.geo.DelimitedArea;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;


@RunWith(MockitoJUnitRunner.class)
public class RestRentalItemServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private RestRentalItemService restRentalItemService;

    @Test
    public void getRentalItems(){
        ReflectionTestUtils.setField(restRentalItemService, "apiUrl", "http://example.com");

        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(), eq(new ParameterizedTypeReference<List<RentalVehicle>>() {}))).thenReturn(mockRentalVehicles());

        List<RentalVehicle> result = restRentalItemService.getRentalVehicles(mockDelimitedArea(), Arrays.asList("111"));

        Assert.assertEquals(1, result.size());

    }

    private ResponseEntity<List<RentalVehicle>>  mockRentalVehicles(){
        ResponseEntity<List<RentalVehicle>> responseEntity = new ResponseEntity<>(Collections.singletonList(
            RentalVehicle.builder()
                .id("example")
                .build()
        ), HttpStatus.OK);

        return responseEntity;
    }

    private DelimitedArea mockDelimitedArea (){
        DelimitedArea delimitedArea = DelimitedArea.builder()
            .lowerLeftLatLon(
                CartesianPoint.builder()
                    .x(38.711046)
                    .y(-9.160096)
                    .build()
            )
            .upperRightLatLon(
                CartesianPoint.builder()
                    .x(38.739429)
                    .y(-9.137115)
                    .build()
            )
            .build();

        return delimitedArea;
    }

}

# Polling app (Meep)

Es una aplicación que hace una petición REST cada 30 segundos para leer que vehículos están disponibles dada una localización delimitada y una lista de compañías (se usan los datos especificados en el enunciado de la práctica). La aplicación escupe por log que vehículos son nuevos respecto a la lectura anterior, y cuales han dejado de estar disponibles. Más adelante anotaciones al respecto.

# Iniciar la aplicación

  - Se ha dockerizado tanto el Spring Boot como la BD de MySQL sobre la que se apoya.
  - Hay que entrar en el directorio docker y levantar los containers (se despliega el .jar localizado en el directorio target, si por lo que sea se borra hay que hacer mvn clean install en el directorio raíz para generarlo)
```sh
$ cd docker
$ docker-compose up
```


### Anotaciones

* Se ha utilizado Spring Batch. El job que se ejecuta cada 30 segundos, consta de 2 steps: El 1º llama a la API, procesa los items leídos uno a uno y acaba persistiendo un registro en la base de datos (implementación Reader+Processor+Writer), y el 2º step, se encarga de borrar (borrado lógico) aquellos items que estaban guardados y que ya no aparecen en la lectura, para poder identificar que vehículos ya no están disponibles. Para poder comparar elementos de una sincronización a otra, el elemento se guarda con un uuid asociado al job ejecutado, el cual será diferente en cada iteración.

* Para comparar unos vehículos con otros he considerado compararlos por el ID.
* La URL se puede cambiar por configuración (.yml), el resto de datos indicados en el enunciado (latitud, longitud...) está a piñón en el código, pero igualmente podrían ser parámetros de entrada configurables desde el .yml o desde BD.
* Se ha intentado aproximar el desarrollo a una arquitectura hexagonal / clean architecture.
* En la entidad de persistencia, para no demorar más, solo están mapeado los campos necesarios para el funcionamiento de la prueba. Por el mismo motivo, se utiliza la entidad de dominio de la aplicación para mapear los datos recibidos de la llamada REST, habría que estudiar si es mejor un VO (y mapear después a la entidad de dominio) dependiendo de los requerimientos.

